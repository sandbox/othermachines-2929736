## Language Fallback Exclude

What it does:

The [entity_translation](https://www.drupal.org/project/entity_translation) module allows administrators to allow or deny language fallback, described as follows:

"When language fallback is allowed, an alternative translation will be displayed if the requested language is not available."

This module further allows administrators to exclude individual nodes from this behaviour. If language fallback is not enabled this module has no effect.

Details:

- Provides a new permission: 'exclude nodes from language fallback'.
- Adds a new property/field to the 'node' table: lfe.
- Adds a checkbox field to the node form (only accessible to users with 'exclude nodes from language fallback' permission).
- Denies access (403) on nodes that are excluded *if a translation isn't provided*.
- Hides (with CSS) menu links attached to nodes that are excluded *if a translation isn't provided*.

To do:

- Check sitemap.xml generation to determine if intervention is needed to exclude these nodes.
