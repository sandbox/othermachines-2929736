<?php

/**
 * @file
 * Code for the Language Fallback Exception module.
 */

/**
 * Preprocess variables for menu_link.
 *
 * If menu link represents a node path and the node is excluded from language
 * fallback and no translation is provided in current language, hide the menu
 * link.
 *
 * @Todo: Footer menu is still showing link.
 */
function lfe_preprocess_menu_link(&$variables) {
  global $language;
  if (variable_get('locale_field_language_fallback', TRUE)) {
    $path = drupal_get_normal_path($variables['element']['#original_link']['link_path']);
    if (arg(0, $path) == 'node' && arg(1, $path) && !arg(2, $path)) {
      $node = node_load(arg(1, $path));
      if ($node) {
        if ($node->language != LANGUAGE_NONE && $node->lfe && !isset($node->translations->data[$language->language])) {
          $variables['element']['#attributes']['style'] = 'display:none;';
        }
      }
    }
  }
}

/**
 * Implements hook_node_access().
 *
 * Note: User 1 and users with 'bypass node access' permission will still be
 * able to view the language fallback.
 */
function lfe_node_access($node, $op, $account = null) {
  global $language;
  if (variable_get('locale_field_language_fallback', TRUE) && $op == 'view') {
    if (!user_access('bypass node access')) {
      // If node is excluded from language fallback, and no translation
      // exists for the current language, deny access.
      if ($node->language != LANGUAGE_NONE && $node->lfe && !isset($node->translations->data[$language->language])) {
        return NODE_ACCESS_DENY;
      }
    }
  }
  return NODE_ACCESS_IGNORE;
}

/**
 * Implements hook_language_switch_links_alter().
 *
 * Alters language switching links for currently loaded node if there is no
 * translation available.
 */
function lfe_language_switch_links_alter(array &$links, $type, $path) {

  if (variable_get('locale_field_language_fallback', TRUE)) {
    // Language fallback is enabled, which means by default all language links
    // will be available. Load them.
    $lang_codes = array_keys($links);
    // Key the array by language code.
    $lang_codes = array_flip($lang_codes);

    // If this is a path to a node, load the node object and check its properties.
    if (arg(0, $path) == 'node' && arg(1, $path) && !arg(2, $path)) {
      $node = node_load(arg(1, $path));
      if ($node->language != LANGUAGE_NONE && $node->lfe) {
        // Node is excluded from language fallback; this will remove a language
        // code from defaults if a translation isn't available for this node.
        $lang_codes = array_keys($node->translations->data);
        $lang_codes = array_flip($lang_codes);
      }
    }

    // Hide links where a translation isn't available.
    foreach ($links as $lang => &$link) {
      if (!isset($lang_codes[$lang])) {
        $link['attributes']['style'] = 'display:none;';
      }
    }
  }
}


/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Add a new fieldset to node forms.
 */
function lfe_form_node_form_alter(&$form, &$form_state, $form_id) {
  if (variable_get('locale_field_language_fallback', TRUE) && user_access('exclude nodes from language fallback')) {

    $form['lfe'] = array(
      '#type' => 'fieldset',
      '#title' => 'Language fallback exclude',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#access' => user_access('exclude nodes from language fallback'),
      '#group' => 'additional_settings',
    );

    $form['lfe']['lfe'] = array(
      '#type' => 'checkbox',
      '#title' => 'Exclude node from language fallback',
      '#description' => t('If no translation is provided, the site will fall back to the original content. Selecting this will disable that behaviour for this node. If a translation is provided, this has no effect.'),
      '#required' => FALSE,
      '#default_value' => isset($form['#node']->lfe) ? $form['#node']->lfe : 0,
    );
 }
}

/**
 * Implements hook_permission().
 */
function lfe_permission() {
  return array(
    'exclude nodes from language fallback' => array(
      'title' => t('Exclude nodes from language fallback'),
    ),
  );
}
